package com.training.pets;

import com.training.Feedable;

public class Pet implements Feedable {

    // static property
    // think of static as an object
    // a singular object that represents the class
    // when the JVM starts up it allocates space for the the pet class
    // and the static property first.
    // static things exist before an instance is created - therefore can be called.
    // none static properties can only be called when an instance has been created.
    public static int numPets = 0;

    // to add getters and setters
    // highlight variables and click code tab, click generate
    // and then select getters and setters
    private int numLegs;
    private String name;

    // constructor - no return type and same name as class
    public Pet() {
        numPets++;
    }

    public int getNumLegs() {
        return numLegs;
    }

    public void setNumLegs(int numLegs) {
        this.numLegs = numLegs;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void feed() {

        System.out.println("Feed generic pet some food");

    }

    public static int getNumPets() {
        return numPets;
    }
}
