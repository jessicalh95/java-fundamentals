package com.training.pets;

import com.training.Feedable;

public class Dog extends Pet implements Feedable {

    // constructor - note no return type and always named the same as the class
    public Dog() {
        this.setNumLegs(4);
    }

    // another constructor
    public Dog(String name) {
        this.setName(name);
        this.setNumLegs(4);
    }

    // control alt l to format
    public void feed() {

        System.out.println("Feed some dog food");
    }

    public void bark() {
        System.out.println("Woof! woof!!!");
    }

    @Override // overrides a parent function
    public String toString() {
        return "This is a dog, with " + this.getNumLegs() + " legs, called " + this.getName();
    }
}
