package com.training.person;

import com.training.Feedable;

public class Person implements Feedable {

    // properties
    private String name;

    // constructors

    /**
     * Default constructor
     */
    public Person() {

    }

    /**
     * Constructor with args
     * @param name of person
     */
    public Person(String name) {
        this.name = name;
    }

    // getters and setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * Method to feed the person
     */
    public void feed() {
        System.out.println("Feed the person - it's dinner time!");
    }

    /**
     * Overridden toString method
     * @return
     */
    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                '}';
    }
}
