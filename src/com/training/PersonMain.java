package com.training;

import com.training.person.Person;

public class PersonMain {

    public static void main(String[] args) {

        // create an instance of the person object
        Person myPerson = new Person("Jose");

        System.out.println(myPerson);

    }
}
