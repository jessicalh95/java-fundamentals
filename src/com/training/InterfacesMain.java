package com.training;

import com.training.person.Person;
import com.training.pets.Dog;

public class InterfacesMain {

    public static void main(String[] args) {

        // create an array of type feedable
        Feedable[] thingsWeCanFeed = new Feedable[5];

        Person horace = new Person("Horace");

        horace.setName("Horace O'Brien");

        Dog dogo = new Dog("Ralph");
        dogo.bark();

        thingsWeCanFeed[0] = horace;
        thingsWeCanFeed[1] = dogo;





    }







}
