package com.training;

import com.training.pets.Dog;
import com.training.pets.Pet;

import java.util.ArrayList;

public class ArraysMain {

    // type psvm or main = shortcut
    public static void main(String[] args) {

        // part of the collections library
        // round brackets - calls the constructor for the ArrayList
        ArrayList<Pet> myPetList = new ArrayList<Pet>();

        // create an array of ints
        int[] intArray = {5, 32, 64, 34, 77};

        // create an array of pets
        Pet[] petArray = new Pet[10];

        // print out the contents of the array
        for (int i = 0; i < petArray.length; ++i) {
            System.out.println(petArray[i]);
        }

        // print out the contents of the array
        for (int i = 0; i < intArray.length; i++) {
            System.out.println(intArray[i]);
        }

        // put a pet in the array
        Pet firstPet = new Pet();
        firstPet.setName("First com.training.pets.Pet");

        petArray[0] = firstPet;

        // put a dog in the array
        Dog myDog = new Dog("Buds");

        petArray[1] = myDog;

        // print out the contents of the array
        for (int i = 0; i < petArray.length; ++i) {
            System.out.println(petArray[i]);

            // feed all pets in the array if not null
            if(petArray[i] != null) {
                petArray[i].feed();
            }
        }

        //petArray[0].feed();
        //petArray[1].feed();

    }

}
