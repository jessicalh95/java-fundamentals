package com.training;

public interface Feedable {

    void feed();

    // anything that implements feedable will have a feed method
    // and will be implemention will be added there
}
