package com.training;

import com.training.pets.Dog;

public class StringMain {

    public static void main(String[] args) {

        String bigString = "start: ";

        // making a reference of type string builder
        StringBuilder stringBuilder = new StringBuilder(bigString);

        for (int i = 0; i < 10; ++i) {
            //bigString += i; - code below is better
            // it is more efficient and better to use for a big loop
            // cpu and memory better
            stringBuilder.append(i);
        }

        // System.out.println(bigString);
        System.out.println(stringBuilder);

        // under the hood the compiler crates multiple strings and appends them
        String testString = "hello " + "world " + "after " + "lunch.";

        System.out.println(testString);

        Dog stringDog = new Dog("Daug");
        // to string method has been added in the Dog class and overridden
        System.out.println("Dog created : " + stringDog.toString());
    }

}
