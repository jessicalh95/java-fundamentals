package com.training;

import com.training.pets.Dog;
import com.training.pets.Pet;

public class Main {

    public static void main(String[] args) {

        System.out.println("Before anything " + Pet.numPets);

        Pet myPet = new Pet();
        myPet.feed();
        System.out.println(myPet.numPets); // 1 will print out

        // set pet name and number of legs
        myPet.setName("Buddy");
        myPet.setNumLegs(4);

        // print pet name and number of legs by using get
        System.out.println("My pets name is " + myPet.getName() + " and he has " + myPet.getNumLegs() + " legs.");

        Dog myDog = new Dog();
        myDog.feed();
        myDog.bark();

        // can also refer to a dog as a pet rather than a dog
        // can refer to it as type pet or dog
        // I now have three objects - a pet of type pet, a dog of type dog, a pet of type dog
        Pet secondDog = new Dog();
        secondDog.feed(); // still uses the feed method from the dog class
        // secondDog.bark(); - because I have referred to the dog as a pet I can't access
        // any functions of the com.training.pets.Dog class, can only use com.training.pets.Pet methods

        // ^^ What's the purpose of this??
        // In python you can have an array or list of different types
        // this is not the case in Java
        // cannot mix an array with pet and dog
        // but if dog extends pet and we refer a dog to a pet then we can add
        // a mix of pet and dog to an array because the dog is referred to a pet in this case

        //com.training.pets.Pet anotherReferenceToDog;
        //anotherReferenceToDog = myDog;

        //myDog.bark();
        // anotherReferenceToDog.bark(); - will not work


    }
}
